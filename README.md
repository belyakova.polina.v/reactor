# Reactor processing

### Start project

```
git clone https://gitlab.com/belyakova.polina.v/reactor.git
```
```
mvn package
```
---

### DB SCHEMA

#### schema for components params
![DB SCHEMA](https://habrastorage.org/webt/jc/ek/sx/jceksxo3my8ik3tmojlqjtad7oe.png)

---

### TODO

 - Create & setup remote Server
 - Create & setup remote DB
 - Create & setup BackEnd API (basic logic)
 - Create & setup FrontEnd project (WEB || mobile)
