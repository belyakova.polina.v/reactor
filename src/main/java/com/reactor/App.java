package com.reactor;

import com.numericalmethod.suanshu.analysis.function.rn2r1.univariate.UnivariateRealFunction;
import com.numericalmethod.suanshu.analysis.uniroot.Newton;
import com.numericalmethod.suanshu.analysis.uniroot.NoRootFoundException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;



public class App {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);

        // Загружаем в память данные по НРТЛ ===ВАЖНО===
        NRTL.readFromJSON();


//        sc.nextInt();
        DBConnector db = new DBConnector();
        List<Map<String, ?>> components = db.GetComponentParams();

        List<TempCalculateComponent> left = new ArrayList<>();
        List<TempCalculateComponent> right = new ArrayList<>();

        System.out.println("Задайте параметры питания, кмоль/ч");
        double feed = sc.nextDouble();
        for (Map<String, ?> c : components) {
            System.out.println(
                    c.get("name")
            );
            TempCalculateComponent component = new TempCalculateComponent(
                    (String) c.get("name"),
                    0,
                    ((BigDecimal) c.get("tempcritical")).doubleValue(),
                    ((BigDecimal) c.get("stenthalpy")).doubleValue(),
                    ((BigDecimal) c.get("stenthalpyofvaporiz")).doubleValue(),
                    ((BigDecimal) c.get("a_d")).doubleValue(),
                    ((BigDecimal) c.get("b_d")).doubleValue(),
                    ((BigDecimal) c.get("c_d")).doubleValue(),
                    ((BigDecimal) c.get("d_d")).doubleValue(),
                    ((BigDecimal) c.get("a_v")).doubleValue(),
                    ((BigDecimal) c.get("b_v")).doubleValue(),
                    ((Long) c.get("a_vap")).doubleValue(),
                    ((BigDecimal) c.get("b_vap")).doubleValue(),
                    ((BigDecimal) c.get("c_vap")).doubleValue(),
                    ((BigDecimal) c.get("d_vap")).doubleValue(),
                    ((BigDecimal) c.get("c1_tb")).doubleValue(),
                    ((BigDecimal) c.get("c2_tb")).doubleValue(),
                    ((BigDecimal) c.get("c3_tb")).doubleValue(),
                    ((BigDecimal) c.get("a_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("b_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("c_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("d_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("e_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("a_cap_v")).doubleValue(),
                    ((BigDecimal) c.get("b_cap_v")).doubleValue(),
                    ((BigDecimal) c.get("c_cap_v")).doubleValue(),
                    ((BigDecimal) c.get("d_cap_v")).doubleValue(),
                    ((BigDecimal) c.get("t_boil")).doubleValue()
            );
            System.out.println("Введите количество компонента");
            component.n_0 = (sc.nextDouble() * feed);
            System.out.println("Введите количество молей в реации");
            component.moles_in_reaction = sc.nextDouble();
            component.calculateAllParameters(323.15);
            if (Objects.equals(component.name, "ethanol")) {
                left.add(component);
            }
            else {
                right.add(component);
            }
        }

        List<Double> P_reactors = new ArrayList<>();
        System.out.println("Введите количество реаций");
        int reaction_count = sc.nextInt();
        System.out.println("Введите Температуру");
        double T = sc.nextDouble();
        double P_init = 10;

        List<Double> V_reaction = new ArrayList<>();
        List<Double> A_left = new ArrayList<>();
        List<Double> E_left = new ArrayList<>();
        List<Double> A_right = new ArrayList<>();
        List<Double> E_right = new ArrayList<>();

        for (int i = 0; i < reaction_count; i++) {
            System.out.println("Реакаци #" + (i + 1));
            System.out.print("V реакции \n>");
            V_reaction.add(sc.nextDouble());
            System.out.print("A реакции левая\n>");
            A_left.add(sc.nextDouble());
            System.out.print("E реакции левая\n>");
            E_left.add(sc.nextDouble());
            System.out.print("A реакции правая\n>");
            A_right.add(sc.nextDouble());
            System.out.print("E реакции правая\n>");
            E_right.add(sc.nextDouble());
        }


        Reactor re = new Reactor();
        P_reactors = re.CalculateReactor(left, right, V_reaction, P_init, T, A_left, E_left, A_right, E_right);
        for (double c: P_reactors) {
            System.out.println(c);
            System.out.print("Производительность по этанолу "  + (feed - c - c) + "\n" );
        }



        System.out.print("Введите количество температур, для которых будет произведен расчёт\n> ");
        int size = sc.nextInt();
        double[] temp = new double[size];
        System.out.println("Введите значения температур (K) для расчёта свойств");
        for (int p = 0; p < size; p++) {
            System.out.print("> ");
            temp[p] = sc.nextDouble();
        }

        List<TempCalculateMixture> mixtures = new ArrayList<>();
        for(double temperature : temp) {
            System.out.println("Для температуры " + temperature);

            // Инициализация новых компонентов и смеси
            TempCalculateMixture mixture = new TempCalculateMixture();
            List<TempCalculateComponent> calcComponents = new ArrayList<>();

            for (Map<String, ?> c : components) {
                System.out.println(
                        c.get("name")
                );
                TempCalculateComponent component = new TempCalculateComponent(
                    (String) c.get("name"),
                    0,
                    ((BigDecimal) c.get("tempcritical")).doubleValue(),
                    ((BigDecimal) c.get("stenthalpy")).doubleValue(),
                    ((BigDecimal) c.get("stenthalpyofvaporiz")).doubleValue(),
                    ((BigDecimal) c.get("a_d")).doubleValue(),
                    ((BigDecimal) c.get("b_d")).doubleValue(),
                    ((BigDecimal) c.get("c_d")).doubleValue(),
                    ((BigDecimal) c.get("d_d")).doubleValue(),
                    ((BigDecimal) c.get("a_v")).doubleValue(),
                    ((BigDecimal) c.get("b_v")).doubleValue(),
                    ((Long) c.get("a_vap")).doubleValue(),
                    ((BigDecimal) c.get("b_vap")).doubleValue(),
                    ((BigDecimal) c.get("c_vap")).doubleValue(),
                    ((BigDecimal) c.get("d_vap")).doubleValue(),
                    ((BigDecimal) c.get("c1_tb")).doubleValue(),
                    ((BigDecimal) c.get("c2_tb")).doubleValue(),
                    ((BigDecimal) c.get("c3_tb")).doubleValue(),
                    ((BigDecimal) c.get("a_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("b_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("c_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("d_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("e_cap_l")).doubleValue(),
                    ((BigDecimal) c.get("a_cap_v")).doubleValue(),
                    ((BigDecimal) c.get("b_cap_v")).doubleValue(),
                    ((BigDecimal) c.get("c_cap_v")).doubleValue(),
                    ((BigDecimal) c.get("d_cap_v")).doubleValue(),
                    ((BigDecimal) c.get("t_boil")).doubleValue()
                );
                component.calculateAllParameters(temperature);

                calcComponents.add(component);
            }
            mixture.setComponents(calcComponents);


            Separator s = new Separator();
            ArrayList<Double> composition = new ArrayList<>();
            for (TempCalculateComponent comp : mixture.getComponents()) {
                System.out.print("Введите состав парожидкостной смеси в мольных долях " + comp.name + " \n> ");
                double mix_composition = sc.nextDouble();
                composition.add(mix_composition);
            }
            System.out.println("Задайте давление для расчета, мм. рт. ст.");
            final double P = sc.nextDouble();
            System.out.println("Задайте параметры питания, кмоль/ч");
            feed = sc.nextDouble();

            List<MyStream> _stream = s.calculateSeparator(mixture, temperature, composition, P, feed);
            for (int i = 0; i < _stream.size(); i++) {
                System.out.println(_stream.get(i).streamVal);
                System.out.println(_stream.get(i).mix);
            }
            double d = sc.nextDouble();


            double calcTemp = 0;
            calcTemp = calcTemp / mixture.getComponents().toArray().length;
            mixtures.add(mixture);
            mixture.setComposition(composition);
            mixture.calculateAllParams(temperature);



            //================================================//
            // Цикл для расчёта температуры кипения смеси
            // при заданном давлении и начальном приближении
            // по температуре
            Newton n = new Newton(0.1E-10, 20);
            final TempCalculateMixture f_mixture = mixture;
            double temp_boiling = n.solve(new UnivariateRealFunction() {
                @Override
                public double evaluate(double v) {
                    Phase phase = null;
                    try {
                        phase = new Phase(f_mixture, v);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    phase.calc_steam_pressure();
                    phase.calc_gamma();
                    return phase.calculate_boil_temperature(P);
                }
            }, calcTemp);

            System.out.println("------");
            System.out.println(temp_boiling);
            System.out.println("------");
            //================================================//

            System.out.println(calcTemp);

            //================================================//
            double condensing_start_temperature = n.solve(new UnivariateRealFunction() {
                @Override
                public double evaluate(double v) {
                    Phase phase = null;
                    try {
                        phase = new Phase(f_mixture, v);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    phase.calc_steam_pressure();
                    phase.calc_distribution_ratio(P);
                    phase.calculate_molar_fraction_liquid_condensation(P);
                    phase.calculate_normalized();
                    phase.calc_gamma_condensing_temp();
                    return phase.calculate_condensing_start_temperature(P);
                }
            }, calcTemp);

            System.out.println("=======");
            System.out.println(condensing_start_temperature);
            System.out.println("=======");
            //================================================//

            double avg_temp = (condensing_start_temperature + temp_boiling) / 2;

            //================================================//
            double fix_temp = n.solve(new UnivariateRealFunction() {
                @Override
                public double evaluate(double v) {
                    Phase phase = null;
                    try {
                        phase = new Phase(f_mixture, v);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    phase.calc_steam_pressure();
                    phase.calc_distribution_ratio_fix_tp(P);
                    try {
                        phase.calculation_betta();
                    }
                    catch (Exception e){
                        System.out.println(e);
                    }
                    phase.calculate_molar_fraction_liquid_fix_tp();
                    phase.calculate_molar_fraction_vapor_fix_tp();
                    phase.calc_gamma_3();
                    phase.calculation_molar_fraction_vapor_new_fix_tp(P);
                    double sum = 0;
                    for (TempCalculateComponent comp: phase.curMixture.getComponents()) {
                        sum += comp.molar_fraction_vapor_new_fix_tp - comp.molar_fraction_vapor_fix_tp;
                    }
                    return sum;
                }
            }, avg_temp);

            System.out.println("=======");
            System.out.println(fix_temp);
            System.out.println("=======");
            //================================================//


        }

    }


}


