package com.reactor;

import java.util.List;

public interface Mixture {

    double getMixDensity(double temp);
    double getMixViscosity(double temp);
    double getMixEnthalpyOfVap (double temp);
    double getMixEnthalpyOfLiq (double temp);
    double getMixEnthalpyOfVaporiz(double temp);
    double getMixPressure (double temp);
    double getMixHeatCapacityOfVap(double temp);
    double getMixHeatCapacityOfLiq(double temp);

    int getNumComp();

    List<Double> getComposition();
    void setComposition(List<Double> composition);

    List<TempCalculateComponent> getComponents();
    void setComponents(List<TempCalculateComponent> components);
}
