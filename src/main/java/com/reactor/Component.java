package com.reactor;

public interface Component {

    double getDensity(double temp);
    double getViscosity(double temp);
    double getEnthalpyOfVaporiz(double temp);
    double getTempBoil(double pressure);
    double getPressure (double temp);
    double getHeatCapacityOfVap(double temp);
    double getHeatCapacityOfLiq(double temp);
    double getEnthalpyOfVap(double temp);
    double getEnthalpyOfLiq(double temp);

    String getName();
    void setName(String name);

    double getMolecMass();
    void setMolecMass(double molecMass);

    double getTempCritical();
    void setTempCritical(double tempCritical);

    double getStEnthalpy();
    void setStEnthalpy(double stEnthalpy);

    double getStEnthalpyOfVaporiz();
    void setStEnthalpyOfVaporiz(double stEnthalpyOfVaporiz);
}
