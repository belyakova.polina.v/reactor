package com.reactor;

import com.numericalmethod.suanshu.analysis.function.rn2r1.univariate.UnivariateRealFunction;
import com.numericalmethod.suanshu.analysis.uniroot.Newton;
import com.numericalmethod.suanshu.analysis.uniroot.NoRootFoundException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class Phase {

    public TempCalculateMixture curMixture;
    public double T;
    public double betta;
    public DBConnector db;

    public Phase(TempCalculateMixture mix, double T) throws IOException {
        this.curMixture = mix;
        this.T = T;
        this.db = new DBConnector();
        NRTL.readFromJSON();
    }
//        Расчет начала кипения

    /**
     *
     */
    public void calc_steam_pressure() {
        List<TempCalculateComponent> components = curMixture.getComponents();
        for (TempCalculateComponent v : components) {
            v.steam_pressure = v.getPressure(T);
        }
    }

    /**
     *
     * @param P
     */
    public void calc_distribution_ratio(double P) {
        List<TempCalculateComponent> components = curMixture.getComponents();
        for (TempCalculateComponent v : components) {
            v.distribution_ratio = v.getPressure(T) / P;
        }
    }

    public void calc_distribution_ratio_fix_tp(double P) {
        List<TempCalculateComponent> components = curMixture.getComponents();
        for (TempCalculateComponent v : components) {
            if (v.gamma_3 == 0) {
                v.distribution_ratio = v.getPressure(T) / P;
            } else {
                v.distribution_ratio = (v.gamma_3 * v.steam_pressure) / P;
            }

        }
    }

    /**
     *
     */
    public void calc_molar_fraction_liquid() {
        List<TempCalculateComponent> components = curMixture.getComponents();
        double s_n_0 = 0;
        for (TempCalculateComponent v : components) {
            s_n_0 += v.n_0;
        }

        for (TempCalculateComponent v : components) {
            v.molar_fraction_liquid = v.n_0 / s_n_0;
        }
    }

    /**
     *
     */
    public void calc_molar_fraction_steam() {
        List<TempCalculateComponent> components = curMixture.getComponents();
        double s_n_0 = 0;
        for (TempCalculateComponent v : components) {
            s_n_0 += v.n_0;
        }

        for (TempCalculateComponent v : components) {
            v.molar_fraction_steam = v.n_0 / s_n_0;
        }
    }

    /**
     *
     */
    public void calc_gamma() {

        List<TempCalculateComponent> components = curMixture.getComponents();

        for (int i = 0; i < components.toArray().length; i++) {
            double left = 0;
            double right = 0;

            double leftUp = 0;
            double leftDown = 0;
            double centerUp = 0;


            TempCalculateComponent first = components.get(i);

            for (int j = 0; j < components.toArray().length; j++) {
                TempCalculateComponent second = components.get(j);
                double rightUp = 0;
                double rightDown = 0;
                double centerDown = 0;
//                      G (ji)
                double Gji = G(second, first);
                double Xj = second.molar_fraction_liquid;
//                Double Xj = curMixture.getComposition().get(j);

//                            tau (ji)             x (j)
                leftUp += tau(second, first) * Xj * Gji;
//                              x (k)
                leftDown += Xj * Gji;

                centerUp = Xj * G(first, second);

                for (int k = 0; k < components.toArray().length; k++) {
                    TempCalculateComponent third = components.get(k);
                    double Gkj = G(third, second);
                    double Xk = components.get(k).molar_fraction_liquid;
//                        Double Xk = curMixture.getComposition().get(k);

                    centerDown += Xk * Gkj;

                    rightUp += tau(third, second) * Xk * Gkj;
                    rightDown += Xk * Gkj;
                }
                right += (centerUp / centerDown) * (tau(first, second) - (rightUp / rightDown) );
            }
            left = leftUp / leftDown;

//
            components.get(i).gamma = Math.exp(left + right);
            System.out.printf("[LOG] Гамма компонента \"%s\" = %s%n", components.get(i).name, components.get(i).gamma);
        }
    }

    public void calc_gamma_condensing_temp() {

        List<TempCalculateComponent> components = curMixture.getComponents();

        for (int i = 0; i < components.toArray().length; i++) {
            double left = 0;
            double right = 0;

            double leftUp = 0;
            double leftDown = 0;
            double centerUp = 0;


            TempCalculateComponent first = components.get(i);

            for (int j = 0; j < components.toArray().length; j++) {
                TempCalculateComponent second = components.get(j);
                double rightUp = 0;
                double rightDown = 0;
                double centerDown = 0;
//                      G (ji)
                double Gji = G(second, first);
                double Xj = second.molar_fraction_liquid_normalized;
//                Double Xj = curMixture.getComposition().get(j);

//                            tau (ji)             x (j)
                leftUp += tau(second, first) * Xj * Gji;
//                              x (k)
                leftDown += Xj * Gji;

                centerUp = Xj * G(first, second);

                for (int k = 0; k < components.toArray().length; k++) {
                    TempCalculateComponent third = components.get(k);
                    double Gkj = G(third, second);
                    double Xk = components.get(k).molar_fraction_liquid_normalized;
//                        Double Xk = curMixture.getComposition().get(k);

                    centerDown += Xk * Gkj;

                    rightUp += tau(third, second) * Xk * Gkj;
                    rightDown += Xk * Gkj;
                }
                right += (centerUp / centerDown) * (tau(first, second) - (rightUp / rightDown) );
            }
            left = leftUp / leftDown;

//
            components.get(i).gamma_condensing_temp = Math.exp(left + right);
            System.out.printf("[LOG] Гамма компонента \"%s\" = %s%n", components.get(i).name, components.get(i).gamma_condensing_temp);
        }
    }

    public void calc_gamma_3() {

        List<TempCalculateComponent> components = curMixture.getComponents();

        for (int i = 0; i < components.toArray().length; i++) {
            double left = 0;
            double right = 0;

            double leftUp = 0;
            double leftDown = 0;
            double centerUp = 0;


            TempCalculateComponent first = components.get(i);

            for (int j = 0; j < components.toArray().length; j++) {
                TempCalculateComponent second = components.get(j);
                double rightUp = 0;
                double rightDown = 0;
                double centerDown = 0;
//                      G (ji)
                double Gji = G(second, first);
                double Xj = second.molar_fraction_liquid_fix_tp;
//                Double Xj = curMixture.getComposition().get(j);

//                            tau (ji)             x (j)
                leftUp += tau(second, first) * Xj * Gji;
//                              x (k)
                leftDown += Xj * Gji;

                centerUp = Xj * G(first, second);

                for (int k = 0; k < components.toArray().length; k++) {
                    TempCalculateComponent third = components.get(k);
                    double Gkj = G(third, second);
                    double Xk = components.get(k).molar_fraction_liquid_fix_tp;
//                        Double Xk = curMixture.getComposition().get(k);

                    centerDown += Xk * Gkj;

                    rightUp += tau(third, second) * Xk * Gkj;
                    rightDown += Xk * Gkj;
                }
                right += (centerUp / centerDown) * (tau(first, second) - (rightUp / rightDown) );
            }
            left = leftUp / leftDown;

//
            components.get(i).gamma_3 = Math.exp(left + right);
            System.out.printf("[LOG] Гамма компонента \"%s\" = %s%n", components.get(i).name, components.get(i).gamma_3);
        }
    }

    /**
     *
     * @param first
     * @param second
     * @return
     */
    public double tau(TempCalculateComponent first, TempCalculateComponent second) {

        try {

            double a = getNrtl(first.name, second.name, "a");
            double b = getNrtl(first.name, second.name, "b");
//            double a = this.db.GetNRTLCount(first.name, second.name, "a");
//            double b = this.db.GetNRTLCount(first.name, second.name, "b");


            if (Objects.equals(first.name, second.name)) return 0;
            return (a + (b / this.T));

        } catch (Exception e) {
            System.out.println(e);
        }

        return 0;
    }

    double getNrtl(String firstName, String secondName, String param) {
        for (int i = 0; i < NRTL.nrtl.length(); i++) {
            JSONObject obj = NRTL.nrtl.getJSONObject(i);
            if (
               obj.getJSONArray("first").toList().contains(firstName.toLowerCase(Locale.ROOT)) &&
               obj.getJSONArray("second").toList().contains(secondName.toLowerCase(Locale.ROOT))
            ){
                return obj.getDouble(param);
            }
        }
        return 0;
//        throw new IllegalArgumentException("не могу найти НРТЛ для данной пары");
    }

    /**
     *
     * @param first
     * @param second
     * @return
     */
    public double G(TempCalculateComponent first, TempCalculateComponent second) {
        try {
            double alpha = getNrtl(first.name, second.name, "al");
//            double alpha = this.db.GetNRTLCount(first.name, second.name, "al");
            double _tau = tau(first, second);

            return Math.exp(-(_tau) * alpha);
        } catch (Exception e) {
            System.out.println(e);
        }

        return 0;
    }

    /**
     *
     * @return
     */
    public double F() {
        double F = 0;
        List<TempCalculateComponent> components = curMixture.getComponents();
        for (TempCalculateComponent component : components) {
            F += component.molar_fraction_steam / (component.distribution_ratio + 1);
        }
        return 0;
    }

    /**
     *
     * @param P
     * @return
     */
    public double calculate_boil_temperature(double P) {
        double temperature = 0;
        List<TempCalculateComponent> components = curMixture.getComponents();
        System.out.printf("[LOG] вычисленная температура = %s%n", T);
        for (TempCalculateComponent component : components) {
            System.out.printf("[LOG] %s%n>gamma = %s%n>molar_fraction_liquid = %s%n>component pressure = %s%n>system presure = %s%n",
                    component.name,
                    component.gamma,
                    component.molar_fraction_liquid,
                    component.getPressure(T),
                    P);
            temperature += (component.gamma * component.molar_fraction_liquid * component.getPressure(T)) / P;
        }
        System.out.printf("[LOG] функция которая должна равняться нулю = %s%n", temperature - 1);
        System.out.println("------------");
        return temperature - 1;
    }

    /**
     *
     */
    public void calculate_molar_fraction_liquid_fix_tp() {
        List<TempCalculateComponent> components = curMixture.getComponents();
        for (TempCalculateComponent component : components) {
            component.molar_fraction_liquid_fix_tp = component.molar_fraction_steam / (this.betta * component.distribution_ratio + 1 - this.betta);
            System.out.printf("[LOG] molar_fraction_liquid_fix_tp = %s%n", component.molar_fraction_liquid_fix_tp);
        }
    }

    /**
     *
     */
    public void calculate_molar_fraction_vapor_fix_tp() {
        List<TempCalculateComponent> components = curMixture.getComponents();
        for (TempCalculateComponent component : components) {
            component.molar_fraction_vapor_fix_tp = component.distribution_ratio * component.molar_fraction_liquid_fix_tp;
        }
    }

    /**
     *
     * @param P
     */
    public void calculation_molar_fraction_vapor_new_fix_tp(double P) {
        List<TempCalculateComponent> components = curMixture.getComponents();
        for (TempCalculateComponent component : components) {
            component.molar_fraction_vapor_new_fix_tp = (component.gamma_3 * component.molar_fraction_liquid_fix_tp * component.steam_pressure) / P;
            System.out.printf("[LOG] molar_fraction_vapor_new_fix_tp = %s%n", component.molar_fraction_vapor_new_fix_tp);
        }
    }

    /**
     *
     * @throws NoRootFoundException
     */
    public void calculation_betta() throws NoRootFoundException {
        Newton n = new Newton(0, 30);
        this.betta = n.solve(new UnivariateRealFunction() {
            @Override
            public double evaluate(double v) {
                double sum = 0;
                List<TempCalculateComponent> components = curMixture.getComponents();
                for (TempCalculateComponent component : components) {
                    sum += component.molar_fraction_steam / (v * component.distribution_ratio + 1 - v);
                }
                sum -= 1;
                return sum;
            }
        }, 0.5);
        System.out.printf("[LOG] Betta = %s%n", this.betta);
    }
//        Начало расчета температуры начала конденсации

    /**
     *
     */
    public void calculate_molar_fraction_liquid_condensation(double P) {
        List<TempCalculateComponent> components = curMixture.getComponents();

        for (TempCalculateComponent v : components) {
            if (v.gamma_condensing_temp == 0) {
                v.molar_fraction_liquid_condensation = v.molar_fraction_steam / v.distribution_ratio;
            } else {
                v.molar_fraction_liquid_condensation = (v.molar_fraction_steam * P) / (v.gamma_condensing_temp * v.steam_pressure);
            }
        }
    }

    /**
     *
     * @param P
     */
    public void calculate_molar_fraction_liquid_condensation_new(double P) {
        List<TempCalculateComponent> components = curMixture.getComponents();

        for (TempCalculateComponent v : components) {

        }
    }


    /**
     *
     */
    public void calculate_normalized() {
        List<TempCalculateComponent> components = curMixture.getComponents();
        double s_molar_fraction_liquid = 0;
        for (TempCalculateComponent v : components) {
            s_molar_fraction_liquid += v.molar_fraction_liquid_condensation;
        }

        for (TempCalculateComponent v : components) {
            //  X normolized                            Xi                          sum Xi
            v.molar_fraction_liquid_normalized = v.molar_fraction_liquid_condensation / s_molar_fraction_liquid;
        }
    }

    /**
     *
     * @param P
     * @return
     */
    public double calculate_condensing_start_temperature(double P) {
        List<TempCalculateComponent> components = curMixture.getComponents();
        double temperature = 0;
        for (TempCalculateComponent v : components) {
            temperature += (v.molar_fraction_steam * P) / (v.gamma_condensing_temp * v.steam_pressure);
        }
        return temperature - 1;
    }


}
