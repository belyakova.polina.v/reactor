package com.reactor;

public final class Settings {
    public static final String DB_HOST = "localhost";
    public static final String DB_USER = "postgres";
    public static final String DB_NAME = "reactor";
    public static final String DB_PORT = "5432";
    public static final String DB_PASSWORD = "12345";
}
