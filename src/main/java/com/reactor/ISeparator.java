package com.reactor;

import com.numericalmethod.suanshu.analysis.uniroot.NoRootFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
public interface ISeparator {
    public List<MyStream> calculateSeparator(TempCalculateMixture mixture, double temp0, ArrayList<Double> mix_composition, double P, double feed) throws NoRootFoundException;
}
