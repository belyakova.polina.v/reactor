package com.reactor;

public class MyStream {
    public TempCalculateMixture mix;
    public double streamVal = 0;

    MyStream(TempCalculateMixture mix, double streamVal) {
        this.mix = mix;
        this.streamVal = streamVal;
    }
}
