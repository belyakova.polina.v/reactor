package com.reactor;

import com.numericalmethod.suanshu.analysis.differentiation.multivariate.Jacobian;
import com.numericalmethod.suanshu.analysis.function.rn2r1.RealScalarFunction;
import com.numericalmethod.suanshu.analysis.function.rn2r1.univariate.UnivariateRealFunction;
import com.numericalmethod.suanshu.analysis.function.rn2rm.RealVectorFunction;
import com.numericalmethod.suanshu.analysis.uniroot.Newton;
import com.numericalmethod.suanshu.analysis.uniroot.NoRootFoundException;
import com.numericalmethod.suanshu.matrix.doubles.linearsystem.LinearSystemSolver;
import com.numericalmethod.suanshu.vector.doubles.Vector;
import com.numericalmethod.suanshu.vector.doubles.dense.DenseVector;

import java.util.Date;


public class NewtonSystemRoot {
    public double accuracy;
    public int maxIter;

    public NewtonSystemRoot (double accuracy, int maxIter)  {
        this.accuracy = accuracy;
        this.maxIter = maxIter;
    }

    /*
     * Searches for a root, x such that f(x) = 0.
     * Parameters:
     * f - a multivariate function
     * guess - an initial guess of the root
     * Returns:
     * an approximate root
     * Throws:
     * NoRootFoundException - when the search fails to find a root
     */
    public Vector solve(final RealScalarFunction[] f, Vector guess) throws NoRootFoundException {
        return solve(
                new RealVectorFunction() {
                    @Override
                    public Vector evaluate(Vector x) {
                        return new DenseVector(x);
                    }

                    @Override
                    public int dimensionOfDomain() {
                        return f[0].dimensionOfDomain();
                    }

                    @Override
                    public int dimensionOfRange() {
                        return f.length;
                    }
                },
                guess
        );
    }

   /*
   * Searches for a root, x such that f(x) = 0.
   * Parameters:
   * f - a system of equations
   * guess - an initial guess of the root
   * Returns:
   * an approximate root
   * Throws:
   * NoRootFoundException - when the search fails to find a root
   */
    public Vector solve(RealVectorFunction f, Vector guess) throws NoRootFoundException {
        Vector curGuess = guess;
        Jacobian curJacobian;
        LinearSystemSolver lss = new LinearSystemSolver(1e-10);
        for(int i = 0; i < maxIter; i++) {
            curJacobian = new Jacobian(f, curGuess);
            LinearSystemSolver.Solution s = lss.solve(curJacobian);
            Vector partSolution = s.getParticularSolution(f.evaluate(curGuess).opposite());
            curGuess = curGuess.add(partSolution);
        }
        return curGuess;
    }
}
