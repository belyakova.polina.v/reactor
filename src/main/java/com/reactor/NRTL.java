package com.reactor;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class NRTL {

    public static JSONArray nrtl = new JSONArray();

    public NRTL() {}

    public static void readFromJSON() throws IOException {
        JSONObject f = FileReader.readJSON("nrtl.json");
        nrtl = f.getJSONArray("data");
    }
}
