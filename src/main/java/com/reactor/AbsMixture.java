package com.reactor;

import java.util.List;

public abstract class AbsMixture implements Mixture {

    protected int numComp;
    protected List<Double> composition;
    protected List<TempCalculateComponent> components;

    public int getNumComp() {
        return numComp;
    }

    public void setNumComp(int numComp) {
        this.numComp = numComp;
    }

    public List<Double> getComposition() {
        return composition;
    }

    public void setComposition(List<Double> composition) {
        this.composition = composition;
    }

    public List<TempCalculateComponent> getComponents() {
        return components;
    }

    public void setComponents(List<TempCalculateComponent> components) {
        setNumComp(components.size());
        this.components = components;
    }

    public abstract double getMixDensity(double temp);
    public abstract double getMixViscosity(double temp);
    public abstract double getMixEnthalpyOfVap (double temp);
    public abstract double getMixEnthalpyOfLiq (double temp);
    public abstract double getMixEnthalpyOfVaporiz(double temp);
    public abstract double getMixPressure (double temp);
    public abstract double getMixHeatCapacityOfVap(double temp);
    public abstract double getMixHeatCapacityOfLiq(double temp);
}
