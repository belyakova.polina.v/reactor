package com.reactor;

import com.numericalmethod.suanshu.analysis.function.rn2r1.univariate.UnivariateRealFunction;
import com.numericalmethod.suanshu.analysis.uniroot.Newton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Reactor implements IReactor{
    @Override
    public List<Double> CalculateReactor(List<TempCalculateComponent> left,
                                         List<TempCalculateComponent> right,
                                         final List<Double> V_reaction,
                                         double P_init,
                                         double T,
                                         List<Double> A_left, List<Double> E_left,
                                         List<Double> A_right, List<Double> E_right) throws Exception {

        Newton n = new Newton(1E-3, 20);
        final List<TempCalculateComponent> _left = left;
        final List<TempCalculateComponent> _right = right;
        final double _T = T;

        List<Double> P_list = new ArrayList<>();

        for(int i = 0; i < V_reaction.size(); i++) {
            final double _v_reaction = V_reaction.get(i);
            final double _A_left = A_left.get(i);
            final double _E_left = E_left.get(i);
            final double _A_right = A_right.get(i);
            final double _E_right = E_right.get(i);

            P_list.add(n.solve(new UnivariateRealFunction() {
                @Override
                public double evaluate(double v) {
                    System.out.println("=============");
                    System.out.println("P = " + v);
                    Reaction re = new Reaction(_left, _right, v, _v_reaction, _T, _A_left, _E_left, _A_right, _E_right, V_reaction.size());
                    re.calc_amount_substance();
                    re.calc_mole_volume();
                    re.calc_volume_flow();
                    re.calc_molar_concentration();
                    double Func_P = re.calc_reactor();
                    System.out.println("Func_P = " + Func_P);
                    return Func_P;
                }
            }, P_init));
        }
        System.out.println("=============");

        return P_list;
    }
}
