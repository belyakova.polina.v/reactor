package com.reactor;

public abstract class AbsComponent implements Component {
    protected final String name;
    protected final double molecMass;
    protected final double tempCritical;
    protected final double stEnthalpy;
    protected final double stEnthalpyOfVaporiz;

    public AbsComponent (String name, double molecMass, double tempCritical, double stEnthalpy, double stEnthalpyOfVaporiz) {
        this.name = name;
        this.molecMass = molecMass;
        this.tempCritical = tempCritical;
        this.stEnthalpy = stEnthalpy;
        this.stEnthalpyOfVaporiz = stEnthalpyOfVaporiz;
    }

    public String getName() { return name; }

    public double getMolecMass() {
        return molecMass;
    }

    public double getTempCritical() { return tempCritical; }

    public double getStEnthalpy() {
        return stEnthalpy;
    }

    public double getStEnthalpyOfVaporiz() {
        return stEnthalpyOfVaporiz;
    }

    public abstract double getDensity(double temp);
    public abstract double getViscosity(double temp);
    public abstract double getEnthalpyOfVaporiz(double temp);
    public abstract double getTempBoil(double pressure);
    public abstract double getPressure (double temp);
    public abstract double getHeatCapacityOfVap(double temp);
    public abstract double getHeatCapacityOfLiq(double temp);
    public abstract double getEnthalpyOfVap(double temp);
    public abstract double getEnthalpyOfLiq(double temp);
}
