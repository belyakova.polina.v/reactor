package com.reactor;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

public class FileReader {
    public static JSONObject readJSON(String filePath) throws IOException {
        File file = new File("./src/main/java/com/reactor", filePath);
        String content = FileUtils.readFileToString(file, "utf-8");

        // Convert JSON string to JSONObject
        return new JSONObject(content);
    }
}
