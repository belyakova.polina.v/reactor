package com.reactor;

public class TempCalculateComponent extends AbsComponent {

    public TempCalculateComponent(String name, double molecMass, double tempCritical, double stEnthalpy, double stEnthalpyOfVaporiz,
                                  double a_d, double b_d, double c_d, double d_d,
                                  double a_v, double b_v,
                                  double a_vap, double b_vap, double c_vap, double d_vap,
                                  double c1_tb, double c2_tb, double c3_tb,
                                  double a_cap_l, double b_cap_l, double c_cap_l, double d_cap_l, double e_cap_l,
                                  double a_cap_v, double b_cap_v, double c_cap_v, double d_cap_v, double t_boil) {

        super(name, molecMass, tempCritical, stEnthalpy, stEnthalpyOfVaporiz);

        this.name = name;

        this.a_d = a_d;
        this.b_d = b_d;
        this.c_d = c_d;
        this.d_d = d_d;

        this.a_v = a_v;
        this.b_v = b_v;

        this.a_vap = a_vap;
        this.b_vap = b_vap;
        this.c_vap = c_vap;
        this.d_vap = d_vap;

        this.c1_tb = c1_tb;
        this.c2_tb = c2_tb;
        this.c3_tb = c3_tb;

        this.a_cap_l = a_cap_l;
        this.b_cap_l = b_cap_l;
        this.c_cap_l = c_cap_l;
        this.d_cap_l = d_cap_l;
        this.e_cap_l = e_cap_l;

        this.a_cap_v = a_cap_v;
        this.b_cap_v = b_cap_v;
        this.c_cap_v = c_cap_v;
        this.d_cap_v = d_cap_v;

        this.t_boil = t_boil;

    }

//    public TempCalculateComponent (double n_0, double moles_in_reaction, double Density)
//    {
//        this.n_0 = n_0;
//        this.moles_in_reaction = moles_in_reaction;
//        this.Density = Density;
//    }

    public TempCalculateComponent(String name, double molecMass, double tempCritical, double stEnthalpy, double stEnthalpyOfVaporiz) {
        super(name, molecMass, tempCritical, stEnthalpy, stEnthalpyOfVaporiz);
    }

    public String name = "";

    public double a_d = 0;
    public double b_d = 0;
    public double c_d = 0;
    public double d_d = 0;

    public double a_v = 0;
    public double b_v = 0;

    public double a_vap = 0;
    public double b_vap = 0;
    public double c_vap = 0;
    public double d_vap = 0;

    public double c1_tb = 0;
    public double c2_tb = 0;
    public double c3_tb = 0;

    public double a_cap_l = 0;
    public double b_cap_l = 0;
    public double c_cap_l = 0;
    public double d_cap_l = 0;
    public double e_cap_l = 0;

    public double a_cap_v = 0;
    public double b_cap_v = 0;
    public double c_cap_v = 0;
    public double d_cap_v = 0;

    public double t_boil = 0;

    public double Density = 0;
    public double Viscosity = 0;
    public double EnthalpyOfVaporiz = 0;
    public double Pressure = 0;
    public double HeatCapacityOfVap = 0;
    public double HeatCapacityOfLiq = 0;
    public double EnthalpyOfVap = 0;
    public double EnthalpyOfLiq = 0;

    public double n_0 = 0;
    public double molar_fraction_liquid = 0;
    public double molar_fraction_steam = 0;

    public double steam_composition = 0;
    public double distribution_ratio = 0;
    public double molar_fraction_liquid_normalized = 0;
    public double molar_fraction_liquid_condensation = 0;
    public double molar_fraction_liquid_fix_tp = 0;
    public double molar_fraction_vapor_fix_tp = 0;
    public double molar_fraction_vapor_new_fix_tp = 0;
    public double distribution_ratio_new_fix_tp = 0;

    // REACTOR VARIABLES
    public double amount_substance = 0;
    public double mole_volume = 0;
    public double molar_concentration = 0;

    public double moles_in_reaction = 0;

    public double steam_pressure = 0;

    public double temp = 0;

    public double gamma = 0;
    public double gamma_condensing_temp = 0;
    public double gamma_3 = 0;
    public double tau = 0;
    public double G = 0;
    public double alpha = 0;
    public double j = 0;


    public void calculateAllParameters(double temp) {
        this.temp = temp;
        Density = getDensity(temp);
        Viscosity = getViscosity(temp);
        EnthalpyOfVaporiz = getEnthalpyOfVaporiz(temp);
        Pressure = getPressure(temp);
        HeatCapacityOfVap = getHeatCapacityOfVap(temp);
        HeatCapacityOfLiq = getHeatCapacityOfLiq(temp);
        EnthalpyOfVap = getEnthalpyOfVap(temp);
        EnthalpyOfLiq = getEnthalpyOfLiq(temp);
    }

    /** Метод для определения плотности
     * ρ = (A)/(B^[1+(1-T/C)^D])
     * @param temp in K units
     * @return density in kmol/cum units
     */
    public double getDensity (double temp){

        return a_d / (Math.pow(b_d, 1 + Math.pow((1 - (temp / c_d)) , d_d)));
    }

    /** Метод для определения вязкости
     * η = 10^(A*((1/T)-(1/B)))
     * @param temp in K units
     * @return viscosity in cPs (сПз) units
     */
    public double getViscosity (double temp){

        return Math.pow(10,a_v *((1/temp) - (1/b_v)));
    }

    /** Метод для определения энтальпии парообразования
     * Hvap = A * (1-θ)^(B+Cθ+Dθ^2)
     * tempCritical in K units
     * @param temp in K units
     * @return enthalpyOfVaporiz in J/kmole units
     */
    public double getEnthalpyOfVaporiz (double temp){
        double tetta = temp/tempCritical;
        return a_vap * ( Math.pow((1 - tetta), b_vap + c_vap*tetta + d_vap * Math.pow(tetta, 2)));
    }

    /** Метод для определения температуры кипения
     *
     * @param pressure in mm Hg units
     * @return tempBoil in K units
     */
    public double getTempBoil (double pressure){

        return (c2_tb/(- Math.log(pressure) + c1_tb)) - c3_tb;
    }

    /** Метод для определения давления
     * P = exp[ c1 - c2/(c3+T)]
     * @param temp in K units
     * @return pressure in mm Hg units
     */
    public double getPressure (double temp){

        return Math.exp(c1_tb - (c2_tb /(c3_tb + temp) ));
    }

    /** Метод для определения темлоемкости пара
     * Cpv = A + B*T + C*T^2 + D*T^3
     * @param temp in K units
     * @return heatCapacityOfVap in kal/(mole * K) units
     */
    public double getHeatCapacityOfVap (double temp){
        return a_cap_v + b_cap_v * temp + c_cap_v * Math.pow(temp, 2) + d_cap_v * Math.pow(temp, 3);
    }

    /** Метод для определения темлоемкости жидкости
     * Cpl = A + B*T + C*T^2 + D*T^3 + E*T^4
     * @param temp in K units
     * @return heatCapacityOfLiq in J/(kmole * K) units
     */
    public double getHeatCapacityOfLiq (double temp){
        return a_cap_l + b_cap_l * temp + c_cap_l * Math.pow(temp, 2) +
                d_cap_l * Math.pow(temp, 3) + e_cap_l * Math.pow(temp, 4);
    }

    /** Метод для определения ?
     * standard enthalpy of formation in kal/mole units
     * @param temp in K units
     * @return enthalpyOfVap in kal/mole units
     */
    public double getEnthalpyOfVap (double temp) {
        return stEnthalpy + a_cap_v * (temp - 298) + b_cap_v * (1/2) *
                (Math.pow(temp, 2) - Math.pow(298, 2)) + c_cap_v * (1/3) *
                (Math.pow(temp, 3) - Math.pow(298, 3)) + d_cap_v * (1/4) *
                (Math.pow(temp, 4) - Math.pow(298, 4));
    }

    /** Метод для определения ?
     * standard enthalpy of formation in kal/mole units,standard enthalpy of vaporization in kal/mole units
     * @param temp in K units
     * @return enthalpyOfLiq in J/kmole units
     */
    public double getEnthalpyOfLiq (double temp) {
        return stEnthalpy * 4184 - stEnthalpyOfVaporiz*4184 + a_cap_l *
                (temp - 298) + b_cap_l * (0.5) *
                (Math.pow(temp, 2) - Math.pow(298, 2)) + c_cap_l * (1/3) *
                (Math.pow(temp, 3) - Math.pow(298, 3)) + d_cap_l * (0.25) *
                (Math.pow(temp, 4) - Math.pow(298, 4)) + e_cap_l * (1/5) *
                (Math.pow(temp, 5) - Math.pow(298, 5));
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void setMolecMass(double molecMass) {

    }

    @Override
    public void setTempCritical(double tempCritical) {

    }

    @Override
    public void setStEnthalpy(double stEnthalpy) {

    }

    @Override
    public void setStEnthalpyOfVaporiz(double stEnthalpyOfVaporiz) {

    }

}

