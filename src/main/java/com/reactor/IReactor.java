package com.reactor;

import java.util.List;

public interface IReactor {
    public List<Double> CalculateReactor(List<TempCalculateComponent> left,
                                         List<TempCalculateComponent> right,
                                         List<Double> V_reaction,
                                         double P_init, double T,
                                         List<Double> A_left, List<Double> E_left,
                                         List<Double> A_right, List<Double> E_right) throws Exception;
}
