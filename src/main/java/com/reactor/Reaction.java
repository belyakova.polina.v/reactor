package com.reactor;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class Reaction {
    List<TempCalculateComponent> LeftComponents;
    List<TempCalculateComponent> RightComponents; 
    List<TempCalculateComponent> AllComponents;
    double P_react;
    public double T;
    public double V_reaction;
    public List<Double> k_reaction;
    public double A_right = 0;
    public double E_right = 0;
    public double A_left = 0;
    public double E_left = 0;
    public int Count_reaction = 0;

    public Reaction(List<TempCalculateComponent> left,
                    List<TempCalculateComponent> right,
                    double P_react, double V_reaction, double T, double A_left, double E_left, double A_right, double E_right, int Count_reaction) {
        this.init(left, right, P_react, V_reaction, T, A_left, E_left, A_right, E_right, Count_reaction);
    }

    public Reaction(TempCalculateComponent[] left,
                    TempCalculateComponent[] right,
                    double P_react, double V_reaction, double T, double A_left, double E_left, double A_right, double E_right, int Count_reaction) {
        new Reaction(Arrays.asList(left), Arrays.asList(right), P_react, V_reaction, T, A_left, E_left, A_right, E_right, Count_reaction);
    }

    double volume_flow = 0;
    double molar_concentration = 0;


    void init(List<TempCalculateComponent> left,
              List<TempCalculateComponent> right,
              double P_react, double V_reaction, double T, double A_left, double E_left, double A_right, double E_right, int Count_reaction) {
        this.LeftComponents = left;
        this.RightComponents = right;
        this.P_react = P_react;
        this.AllComponents = new ArrayList<>();
        this.AllComponents.addAll(this.LeftComponents);
        this.AllComponents.addAll(this.RightComponents);
        this.V_reaction = V_reaction;
        this.T = T;
        this.A_left = A_left;
        this.E_left = E_left;
        this.A_right = A_right;
        this.E_right = E_right;
        this.Count_reaction = Count_reaction;
    }

    public void calc_amount_substance() {
        for (TempCalculateComponent t: this.AllComponents) {
            double sum_mu_P_react = 0;
            double mu_P_react = 0;
            for(int i = 0; i < this.Count_reaction; i++) {
                mu_P_react = t.moles_in_reaction * P_react;
                sum_mu_P_react += mu_P_react;
            }
            t.amount_substance = t.n_0 + sum_mu_P_react;
        }
    }


    public void calc_mole_volume() {
        for (TempCalculateComponent t: this.AllComponents) {
            double s  = t.getDensity(T);
            t.mole_volume = 1 / t.getDensity(T);
        }
    }
    public void calc_volume_flow() {
        for (TempCalculateComponent t: this.AllComponents) {
            volume_flow += (t.mole_volume * t.amount_substance);
        }
    }

    public void calc_molar_concentration() {
        for (TempCalculateComponent t: this.AllComponents) {
            t.molar_concentration = t.amount_substance / volume_flow;
        }
    }

    public double calc_reactor() {
        double conc_for_direct_react = 1;
        for (TempCalculateComponent t : this.LeftComponents) {
            conc_for_direct_react *= Math.pow(t.molar_concentration, Math.abs(t.moles_in_reaction));
        }

        double conc_for_revers_react = 1;
        for (TempCalculateComponent t : this.RightComponents) {
            conc_for_revers_react *= Math.pow(t.molar_concentration, Math.abs(t.moles_in_reaction));
        }
        System.out.println("[INFO] Left = " + V() * K_left() * conc_for_direct_react);
        System.out.println("[INFO] Right = " + V() * K_right() * conc_for_revers_react);
        System.out.println("---------------");
        System.out.println("[INFO] func = " + (P_react - V() * K_left() * conc_for_direct_react + V() * K_right() * conc_for_revers_react));
        System.out.println("---------------");
        return P_react - V() * K_left() * conc_for_direct_react + V() * K_right() * conc_for_revers_react;
    }

    public double V() {
        if (GlobalVariables.betta == 0) {
            return this.V_reaction;
        } else {
            return this.V_reaction * (1 - GlobalVariables.betta);
        }
    }

    private double K_left() {
        double K = this.A_left * Math.exp(- this.E_left / (8.314 * this.T));
        return K;
    }

    private double K_right() {
        double K = this.A_right * Math.exp(- this.E_right / (8.314 * this.T));
        return K;
    }
}
