package com.reactor;

import com.numericalmethod.suanshu.analysis.function.rn2r1.univariate.UnivariateRealFunction;
import com.numericalmethod.suanshu.analysis.uniroot.Newton;
import com.numericalmethod.suanshu.analysis.uniroot.NoRootFoundException;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Separator implements ISeparator{
    @Override
    public List<MyStream> calculateSeparator(TempCalculateMixture mixture, double temp0,  ArrayList<Double> mix_composition, double P, double feed) throws NoRootFoundException {
        double calcTemp = 0;
        int i = 0;
        mixture.feed = feed;
        for (TempCalculateComponent comp: mixture.getComponents()) {
            comp.molar_fraction_steam = mix_composition.get(i);
            comp.molar_fraction_liquid = mix_composition.get(i);
            calcTemp += comp.t_boil;
            i++;
        }
        i = 0;
        calcTemp = calcTemp / mixture.getComponents().size();

        mixture.setComposition(mix_composition);
//        mixture.calculateAllParams(temp0);
        final double _P = P;
        //================================================//
        // Цикл для расчёта температуры кипенмя смеси
        // при заданном давлении и начальном приближении
        // по температуре
        Newton n = new Newton(0.1E-10, 20);
        final TempCalculateMixture f_mixture = mixture;
        double temp_boiling = n.solve(new UnivariateRealFunction() {
            @Override
            public double evaluate(double v) {
                Phase phase = null;
                try {
                    phase = new Phase(f_mixture, v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                phase.calc_steam_pressure();
                phase.calc_gamma();
                return phase.calculate_boil_temperature(_P);
            }
        }, calcTemp);

        System.out.println("------");
        System.out.println(temp_boiling);
        System.out.println("------");
        //================================================//

        if (temp_boiling >= temp0) {
            MyStream vaporStream = new MyStream(mixture, feed * 0);
            MyStream liquidStream = new MyStream(mixture, feed);
            List<MyStream> result = new ArrayList<>();
            result.add(liquidStream);
            result.add(vaporStream);
            return result;
        }


        double molar_fraction_liquid_condensation_latest = 0;
        //================================================//
        double condensing_start_temperature = n.solve(new UnivariateRealFunction() {
            @Override
            public double evaluate(double v) {
                Phase phase = null;
                try {
                    phase = new Phase(f_mixture, v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                phase.calc_steam_pressure();
                phase.calc_distribution_ratio(_P);
                phase.calculate_molar_fraction_liquid_condensation(_P);
                phase.calculate_normalized();
                phase.calc_gamma_condensing_temp();
                return phase.calculate_condensing_start_temperature(_P);
            }
        }, calcTemp);

        System.out.println("=======");
        System.out.println(condensing_start_temperature);
        System.out.println("=======");
        //================================================//

        if (temp0 >= condensing_start_temperature) {
            MyStream vaporStream = new MyStream(mixture, feed);
            MyStream liquidStream = new MyStream(mixture, feed * 0);
            List<MyStream> result = new ArrayList<>();
            result.add(liquidStream);
            result.add(vaporStream);
            return result;
        }

        double avg_temp = (condensing_start_temperature + temp_boiling) / 2;

        //================================================//
        double betta = n.solve(new UnivariateRealFunction() {
            @Override
            public double evaluate(double v) {
                Phase phase = null;
                try {
                    phase = new Phase(f_mixture, v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                phase.calc_steam_pressure();
                phase.calc_distribution_ratio_fix_tp(_P);
                try {
                    phase.calculation_betta();
                }
                catch (Exception e){
                    System.out.println(e);
                }
                phase.calculate_molar_fraction_liquid_fix_tp();
                phase.calculate_molar_fraction_vapor_fix_tp();
                phase.calc_gamma_3();
                phase.calculation_molar_fraction_vapor_new_fix_tp(_P);
                double sum = 0;
                for (TempCalculateComponent comp: phase.curMixture.getComponents()) {
                    sum += comp.molar_fraction_vapor_new_fix_tp - comp.molar_fraction_vapor_fix_tp;
                }
                return sum;
            }
        }, avg_temp);

        GlobalVariables.betta = betta;

        System.out.println("=======");
        System.out.println(betta);
        System.out.println("=======");
        //================================================//

        MyStream vaporStream = new MyStream(mixture, feed);
        MyStream liquidStream = new MyStream(mixture, feed);
        List<MyStream> result = new ArrayList<>();
        result.add(liquidStream);
        result.add(vaporStream);
        return result;
    }
}
