package com.reactor;

import java.math.BigDecimal;
import java.sql.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBConnector {
    protected Connection current_connection;

    public DBConnector() {
        current_connection = getConnection();
    }

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(
                    String.format("jdbc:postgresql://%s:%s/%s", Settings.DB_HOST, Settings.DB_PORT, Settings.DB_NAME),
                    Settings.DB_USER,
                    Settings.DB_PASSWORD
            );

        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }


    public List<Map<String, ?>> GetComponentParams() throws SQLException {
        String sqlQuery = "select c.name, p.* from params p inner join component c on p.component_id = c.id";
        while (current_connection == null) current_connection = this.getConnection();
        Statement s = current_connection.createStatement();
        if (s != null) {
            ResultSet rs = s.executeQuery(sqlQuery);
            return rsToList(rs);
        } else {
            return null;
        }
    }

    public double GetNRTLCount(String first, String second, String param) throws Exception {
        String sqlQuery = String.format("select count from nrtl where name = '%s' and connection = '%s' and param = '%s'", first, second, param);
        while (current_connection == null) current_connection = this.getConnection();
        Statement s = current_connection.createStatement();

        if (s != null) {
            ResultSet rs = s.executeQuery(sqlQuery);
//            System.out.println(rs);
            List<Map<String, ?>> MapList = rsToList(rs);
//            System.out.println(MapList);
            if (MapList.toArray().length > 0) {
                return ((BigDecimal) MapList.get(0).get("count")).doubleValue();
            } else {
                return 0;
//                throw new Exception("error arguments");
            }
        } else {
            return 0;
        }
    }

    public static List<Map<String, ?>> rsToList(ResultSet rs)
            throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        List<Map<String, ?>> results = new ArrayList<Map<String, ?>>();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<String, Object>();
            for (int i = 1; i <= columns; i++) {
                row.put(md.getColumnLabel(i), rs.getObject(i));
            }
            results.add(row);
        }
        return results;
    }

    public static void addNewComponent() throws SQLException {

    }
}
