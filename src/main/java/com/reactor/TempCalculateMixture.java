package com.reactor;

import java.util.List;

public class TempCalculateMixture extends AbsMixture {

    public double mixDensity = 0;
    public double mixViscosity = 0;
    public double mixEnthalpyOfVap = 0;
    public double mixEnthalpyOfLiq = 0;
    public double mixEnthalpyOfVaporiz = 0;
    public double mixPressure = 0;
    public double mixHeatCapacityOfVap = 0;
    public double mixHeatCapacityOfLiq = 0;
    public double feed = 0;

    public void calculateAllParams(double temp) {
        mixDensity = getMixDensity(temp);
        mixViscosity = getMixViscosity(temp);
        mixEnthalpyOfVap = getMixEnthalpyOfVap(temp);
        mixEnthalpyOfLiq = getMixEnthalpyOfLiq(temp);
        mixEnthalpyOfVaporiz = getMixEnthalpyOfVaporiz(temp);
        mixPressure = getMixPressure(temp);
        mixHeatCapacityOfVap = getMixHeatCapacityOfVap(temp);
        mixHeatCapacityOfLiq = getMixHeatCapacityOfLiq(temp);
    }

    /** Метод для определения плотности смеси
     *
     * @param temp in K units
     * @return density in  units
     */
    public double getMixDensity(double temp) {
        double mixDensity = 0;
        for (int i = 0; i < numComp; i++)
            mixDensity += (composition.get(i)) / (components.get(i).getDensity(temp));
        return 1 / mixDensity;
    }

    /** Метод для определения вязкости смеси
     *
     * @param temp in K units
     * @return mixViscosity in cPs (сПз) units
     */
    public double getMixViscosity(double temp) {
        double mixViscosity = 0;
        for (int i = 0; i < numComp; i++)
            for (int j = 0; j < numComp; j++)
            mixViscosity += composition.get(i) * composition.get(i) *
                    Math.log(components.get(i).getViscosity(temp));
        return mixViscosity;
    }

    /** Метод для определения энтальпии пара смеси
     *
     * @param temp in K units
     * @return mixEnthalpyOfVap in kal/mole units
     */
    public double getMixEnthalpyOfVap (double temp) {
        double mixEnthalpyOfVap = 0;
        for (int i = 0; i < numComp; i++)
            mixEnthalpyOfVap += (1 - composition.get(i)) *
                    (components.get(i).getEnthalpyOfVap(temp));
        return mixEnthalpyOfVap;
    }

    /** Метод для определения энтальпии жидкости смеси
     *
     * @param temp in K units
     * @return mixEnthalpyOfLiq in kal/mole units
     */
    public double getMixEnthalpyOfLiq (double temp) {
        double mixEnthalpyOfLiq = 0;
        for (int i = 0; i < numComp; i++)
            mixEnthalpyOfLiq += composition.get(i) *
                    (components.get(i).getEnthalpyOfLiq(temp));
        return mixEnthalpyOfLiq;
    }

    /** Метод для определения энтальпии парообразования смеси
     *
     * tempCritical in K units
     * @param temp in K units
     * @return mixEnthalpyOfVaporiz in kal/mole units
     */
    public double getMixEnthalpyOfVaporiz(double temp){
        double mixMixEnthalpyOfVaporiz = 0;
        for (int i = 0; i < numComp; i++)
            mixMixEnthalpyOfVaporiz += composition.get(i) *
                    (components.get(i).getEnthalpyOfVaporiz(temp));
        return mixMixEnthalpyOfVaporiz;
    }

    /** Метод для определения давления смеси
     *
     * @param temp in K units
     * @return mixPressure in mm Hg units
     */
    public double getMixPressure (double temp) {
        double mixPressure = 0;
        for (int i = 0; i < numComp; i++)
            mixPressure += composition.get(i) *
                    (components.get(i).getPressure(temp));
        return mixPressure;
    }

    /** Метод для определения темлоемкости пара смеси
     *
     * @param temp in K units
     * @return mixHeatCapacityOfVap in kal/(mole * K) units
     */
    public double getMixHeatCapacityOfVap(double temp){
        double mixHeatCapacityOfVap = 0;
        for (int i = 0; i < numComp; i++)
            mixHeatCapacityOfVap += composition.get(i) *
                    (components.get(i).getHeatCapacityOfVap(temp));
        return mixHeatCapacityOfVap;
    }

    /** Метод для определения темлоемкости жидкости смеси
     * @param temp in K units
     * @return mixHeatCapacityOfLiq in kal/(mole * K) units
     */
    public double getMixHeatCapacityOfLiq(double temp){
        double mixHeatCapacityOfLiq = 0;
        for (int i = 0; i < numComp; i++)
            mixHeatCapacityOfLiq += composition.get(i) *
                    (components.get(i).getHeatCapacityOfLiq(temp));
        return mixHeatCapacityOfLiq;
    }


}
