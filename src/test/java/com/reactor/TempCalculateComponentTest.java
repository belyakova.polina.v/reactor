package com.reactor;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;


public class TempCalculateComponentTest {

    private TempCalculateComponent tempCalculateComponent;

    public void setUp() throws Exception {
        // tempCalculateComponent = new TempCalculateComponent();
    }

    public void tearDown() throws Exception {
    }

    public void getDensity() {
    }

    public void getViscosity() {
    }

    public void getEnthalpyOfVaporiz() {
    }

    public void getTempBoil() {
    }

    public void getPressure() {
    }

    public void getHeatCapacityOfVap() {
    }

    public void getHeatCapacityOfLiq() {
    }

    public void getEnthalpyOfVap() {
    }

    public void getEnthalpyOfLiq() {
    }
}